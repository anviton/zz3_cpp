#ifndef nuage_h
#define nuage_h
#include<sstream>
#include<vector>

#include "point.hpp"
#include "cartesien.hpp"

class Point;

class Nuage
{
    private:
        std::vector<Point *> vecteur;

    public:
        using iterator = std::vector<Point*>::iterator;
        using const_iterator = std::vector<Point*>::const_iterator;
        Nuage();
        int size() const;
        void ajouter(Point &pt);
        std::vector<Point*>::iterator begin();
        std::vector<Point*>::iterator end();
        Cartesien barycentre();
        ~Nuage();
};


#endif