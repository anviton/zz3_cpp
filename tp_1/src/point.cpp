#include "point.hpp"


std::ostream & operator<<(std::ostream &o, Point const &p){
    p.afficher(o);
    return o;
}