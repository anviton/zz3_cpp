#include "nuage.hpp"

Nuage::Nuage()
{
}

Nuage::~Nuage()
{
}

int Nuage::size() const {
    return vecteur.size();
}

void Nuage::ajouter(Point &pt){
    vecteur.push_back(&pt);
}
std::vector<Point*>::iterator Nuage::begin() {
    return vecteur.begin();
}

std::vector<Point*>::iterator Nuage::end() {
    return vecteur.end();
}

Cartesien Nuage::barycentre(){
    std::vector<Point*>::iterator it = vecteur.begin();
    double xg = 0, yg = 0;
    Cartesien c;
    while (it!=vecteur.end()){
        if (Cartesien* pt = dynamic_cast<Cartesien*>(*(it))) {
            xg = xg + pt->getX();
            yg = yg + pt->getY();
        } else {
            if(Polaire* pt = dynamic_cast<Polaire*>(*(it))){
                pt->convertir(c);
                xg = xg + c.getX();
                yg = yg + c.getY();
            }else{
                // penser à remonter un code d'erreur ou une exception ne pas juste ignorer le problème
            }

        }
        it++;
    }
    return Cartesien(xg/vecteur.size(), yg/vecteur.size());
}