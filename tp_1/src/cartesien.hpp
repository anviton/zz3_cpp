#ifndef cartesien_h
#define cartesien_h

#include "point.hpp"
#include "polaire.hpp"

class Polaire;

class Cartesien: public Point {
    double x;
    double y;

    public:
        Cartesien(double a, double d);
        Cartesien();
        Cartesien(Polaire const &polaire);
        double getX() const;
        double getY() const;
        void setX(double x);
        void setY(double y);
        void afficher(std::ostream & flux) const override;
        void convertir(Polaire & polaire) const override;
        void convertir(Cartesien & cartesien) const override;
        
        
    

}; 

#endif