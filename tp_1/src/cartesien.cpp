#include <iostream>
#include "cartesien.hpp"

Cartesien::Cartesien(double x, double y):
    x(x), y(y)
{

}

Cartesien::Cartesien():
    x(0), y(0)
{

}

Cartesien::Cartesien(Polaire const &polaire):
    x(polaire.getDistance() * std::cos(polaire.getAngle() * M_PI / 180)), y(polaire.getDistance() * std::sin(polaire.getAngle() * M_PI / 180))
{

}

double Cartesien::getX() const{
    return x;
}

double Cartesien::getY() const{
    return y;
}

void Cartesien::setX(double x) {
    this->x = x;
}

void Cartesien::setY(double y) {
    this->y = y;
}

void Cartesien::afficher(std::ostream & flux) const
{
    flux << "(x=" << x << ";y="<< y << ")";
}

void Cartesien::convertir(Polaire & polaire) const{
    polaire.setAngle(std::atan2(y, x) * 180 / M_PI);
    polaire.setDistance(std::hypot(x, y));
}

void Cartesien::convertir(Cartesien & cartesien) const{
    cartesien.setX(x);
    cartesien.setY(y);
}