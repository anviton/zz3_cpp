#include <iostream>
#include "polaire.hpp"

Polaire::Polaire():
    angle(0), distance(0)
{

}

Polaire::Polaire(double a, double d):
    angle(a), distance(d)
{
    
}

Polaire::Polaire(Cartesien const &cartesien):
    angle(std::atan2(cartesien.getY(), cartesien.getX()) * 180 / M_PI), distance(std::hypot(cartesien.getX(), cartesien.getY()))
{
    
}

void Polaire::afficher(std::ostream & flux) const
{
    flux << "(a=" << angle << ";d="<< distance << ")";
}

double Polaire::getAngle() const{
    return angle;
}

double Polaire::getDistance() const{
    return distance;
}

void Polaire::setAngle(double a){
    angle = a;
}

void Polaire::setDistance(double d){
    distance = d;
}

void Polaire::convertir(Cartesien & cartesien) const{
    cartesien.setX(distance * std::cos(angle * M_PI / 180));
    cartesien.setY(distance * std::sin(angle * M_PI / 180));
}

void Polaire::convertir(Polaire & polaire) const{
    polaire.setDistance(distance);
    polaire.setAngle(angle);
} 