#ifndef polaire_h
#define polaire_h

#include <cmath>
#include "point.hpp"
#include "cartesien.hpp"

class Cartesien;

class Polaire: public Point {
    double angle;
    double distance;
    public:
        Polaire();
        Polaire(double a, double d);
        Polaire(Cartesien const &cartesien);
        void afficher(std::ostream & flux) const override;
        double getAngle() const;
        double getDistance() const;
        void setAngle(double a);
        void setDistance(double d);
        void convertir(Polaire & polaire) const override;
        void convertir(Cartesien & cartesien) const override;


}; 

#endif