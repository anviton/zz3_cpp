#ifndef point_h
#define point_h
#include<sstream>

class Cartesien;
class Polaire;

class Point {
    
    public:
        virtual void afficher(std::ostream & flux) const = 0;
        virtual void convertir(Cartesien & cartesien) const = 0;
        virtual void convertir(Polaire & polaire) const = 0;

};

std::ostream & operator<<(std::ostream &o, Point const &p);

#endif